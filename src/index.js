import "./assets/styles/styles.scss";
import "./index.scss";

const title = document.querySelector("h1");
const subtitle = document.querySelector(".subtitle");
const txt = "Quentin Decoopman";

function typewriter(text, index) {
  if (index > 5) subtitle.classList.add("active");
  if (index < text.length) {
    setTimeout(() => {
      title.innerHTML += `<span>${text[index]}</span>`;
      typewriter(text, index + 1);
    }, 200);
  }
}
setTimeout(() => {
  typewriter(txt, 0);
}, 200);

const ratio = 0.1;

const options = {
  root: null,
  rootMargin: "0px",
  threshold: 0.1,
};

const handleIntersect = function (entries, observer) {
  entries.forEach(function (entry) {
    if (entry.intersectionRatio > ratio) {
      entry.target.classList.remove("reveal");
      observer.unobserve(entry.target);
    }
  });
};

document.documentElement.classList.add("reveal-loaded");
window.addEventListener("DOMContentLoaded", function () {
  const observer = new IntersectionObserver(handleIntersect, options);
  document.querySelectorAll(".reveal").forEach(function (r) {
    observer.observe(r);
  });
});
